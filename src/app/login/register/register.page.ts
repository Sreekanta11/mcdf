import { AlertController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  phone: any;
  access_token: any;
  registrationForm = new FormGroup({
    'name': new FormControl('',Validators.required),
    'phone_no': new FormControl('',Validators.compose([
      Validators.required,
      Validators.maxLength(10),
      Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])),
    'email': new FormControl('',Validators.compose([
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
    'password': new FormControl('',Validators.required)
  });

  constructor(
  ) {}

  ngOnInit() {
  }


}
