import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  phone: any;
  access_token: any;

  loginForm = new FormGroup({
    'phone_no': new FormControl('',Validators.compose([
      Validators.required,
      Validators.maxLength(10),
      Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")])),
    'password': new FormControl('',Validators.required)
  });

  constructor(
  ) {}

  ngOnInit() {
  }
  
}