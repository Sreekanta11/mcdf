import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormdesignPageRoutingModule } from './formdesign-routing.module';

import { FormdesignPage } from './formdesign.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormdesignPageRoutingModule
  ],
  declarations: [FormdesignPage]
})
export class FormdesignPageModule {}
