import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormdesignPage } from './formdesign.page';

const routes: Routes = [
  {
    path: '',
    component: FormdesignPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormdesignPageRoutingModule {}
