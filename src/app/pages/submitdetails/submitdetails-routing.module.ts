import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmitdetailsPage } from './submitdetails.page';

const routes: Routes = [
  {
    path: '',
    component: SubmitdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmitdetailsPageRoutingModule {}
