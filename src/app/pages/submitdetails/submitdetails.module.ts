import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubmitdetailsPageRoutingModule } from './submitdetails-routing.module';

import { SubmitdetailsPage } from './submitdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SubmitdetailsPageRoutingModule
  ],
  declarations: [SubmitdetailsPage]
})
export class SubmitdetailsPageModule {}
