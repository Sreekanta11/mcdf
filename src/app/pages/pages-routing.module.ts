import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesPage } from './pages.page';

const routes: Routes = [
  {
    path: '',
    component: PagesPage
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home-routing.module').then( m => m.HomePageRoutingModule)
  },
  {
    path: 'masterdata',
    loadChildren: () => import('./masterdata/masterdata-routing.module').then( m => m.MasterdataPageRoutingModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user-routing.module').then( m => m.UserPageRoutingModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./submitdetails/submitdetails.module').then( m => m.SubmitdetailsPageModule)
  },
  {
    path: 'passbook',
    loadChildren: () => import('./passbook/passbook.module').then( m => m.PassbookPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./notification/notification.module').then( m => m.NotificationPageModule)
  },
  {
    path: 'formdesign',
    loadChildren: () => import('./formdesign/formdesign.module').then( m => m.FormdesignPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesPageRoutingModule {}
