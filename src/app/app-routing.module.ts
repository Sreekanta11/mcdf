import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./login/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'forgotpassword',
    loadChildren: () =>
      import('./login/forgotpassword/forgotpassword.module').then(
        (m) => m.ForgotpasswordPageModule
      ),
  },
  {
    path: 'blank',
    loadChildren: () => import('./login/blank/blank.module').then( m => m.BlankPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'pages',
    loadChildren: () => import('./pages/pages.module').then( m => m.PagesPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home-routing.module').then( m => m.HomePageRoutingModule)
  },
  {
    path: 'masterdata',
    loadChildren: () => import('./pages/masterdata/masterdata-routing.module').then( m => m.MasterdataPageRoutingModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./pages/user/user-routing.module').then( m => m.UserPageRoutingModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./pages/submitdetails/submitdetails-routing.module').then( m => m.SubmitdetailsPageRoutingModule)
  },
  {
    path: 'passbook',
    loadChildren: () => import('./pages/passbook/passbook-routing.module').then( m => m.PassbookPageRoutingModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile-routing.module').then( m => m.ProfilePageRoutingModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./pages/notification/notification-routing.module').then( m => m.NotificationPageRoutingModule)
  },
  {
    path: 'form',
    loadChildren: () => import('./pages/formdesign/formdesign-routing.module').then( m => m.FormdesignPageRoutingModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
